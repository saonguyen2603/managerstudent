package org.example;

import java.util.Date;
import java.util.Scanner;

public class SinhVien{
    private int id;
    private String name;
    private String code;
    private int age;
    private String phone;
    private String address;
    private Date created_at;
    private Date updated_at;

    public SinhVien() {

    }

    public SinhVien(int id, String name, String code, int age, String phone, String address, Date created_at, Date updated_at) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.age = age;
        this.phone = phone;
        this.address = address;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public  int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public  String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    //input cho insert dữ liệu
    public static SinhVien add() {
        Valiadate chx = new Valiadate();
        Scanner sc = new Scanner(System.in);

        System.out.print("1.2 Tên: ");
        String name = sc.nextLine();
        name = chx.reName(name);


        System.out.print("1.3 Code: ");
        String code = "";
        chx.validateCode();
        code = chx.getCode();



        System.out.print("1.4. Tuổi: ");
        int age = 0;
        age = chx.checkAge(age);

        System.out.print("1.5: Số điện thoại: ");
        String phone = sc.nextLine();

        System.out.print("1.6: Địa chỉ: ");
        String address = sc.nextLine();

        Date created_at = new Date();

        Date updated_at = new Date();

        SinhVien a = new SinhVien();
        a.setName(name);
        a.setCode(code);
        a.setAddress(address);
        a.setAge(age);
        a.setPhone(phone);
        a.setCreated_at(created_at);
        a.setUpdated_at(updated_at);
        return a;
    }

    //input cho update dữ liệu
    public static SinhVien update() {
        Valiadate chx = new Valiadate();
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập lại Id: ");
        int id = sc.nextInt();

        System.out.println("Nhập tên trên 10 ký tự.");
        System.out.print("1.2 Tên: ");
        String name = "";
        name = chx.reName(name);


        System.out.print("1.3 Code: ");
        String code = "";
        code = chx.checkCodeSpecialCharacter1(code);

        System.out.print("1.4. Tuổi: ");
        int age = 0;
        age = chx.checkAge(age);

        System.out.print("1.5: Số điện thoại: ");
        String phone = sc.nextLine();

        System.out.print("1.6: Địa chỉ: ");
        String address = sc.nextLine();

        Date created_at = new Date();

        Date updated_at = new Date();

        SinhVien a = new SinhVien();
        a.setId(id);
        a.setName(name);
        a.setCode(code);
        a.setAddress(address);
        a.setAge(age);
        a.setPhone(phone);
        a.setCreated_at(created_at);
        a.setUpdated_at(updated_at);
        return a;
    }

    public void display(){
        System.out.println("{ID:" + getId() + ", Tên SV: " + getName() + ", Code: " + getCode() +
                ", Tuổi: " + getAge() + ", SĐT: " + getPhone() + ", Địa chỉ: " + getAddress() + "}");
    }
}