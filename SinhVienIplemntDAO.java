package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SinhVienIplemntDAO implements SinhVienDAO {
    private Connection con;

    public SinhVienIplemntDAO(Connection con) {
        this.con = con;
    }

    @Override
    public List<SinhVien> getAll() {
        List<SinhVien> result;
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * From Students");
            result = ShowInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public List<SinhVien> ShowInfo(ResultSet rs) {
        List<SinhVien> result = new ArrayList<>();
        try {
            while (rs.next()) {
                SinhVien p = new SinhVien(rs.getInt("id"), rs.getString("name"),
                        rs.getString("code"), rs.getInt("age"), rs.getString("phone"), rs.getString("address"),
                        rs.getDate("created_at"), rs.getDate("updated_at"));
                result.add(p);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    @Override
    public void insert(SinhVien p) {
        try {
            PreparedStatement pstm1 = con.prepareStatement
                    ("INSERT INTO Students(name , code, age, phone, address, created_at, updated_at) " +
                            "values(?, ?, ?, ?, ?, ?, ?)");
            pstm1.setString(1, p.getName());
            pstm1.setString(2, p.getCode());
            pstm1.setInt(3, p.getAge());
            pstm1.setString(4, p.getPhone());
            pstm1.setString(5, p.getAddress());
            pstm1.setDate(6, new Date(p.getCreated_at().getTime()));
            pstm1.setDate(7, new Date(p.getUpdated_at().getTime()));
            pstm1.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(SinhVien p) {
        try {
            PreparedStatement pstm1 = con.prepareStatement
                    ("update Students set name=?,code=?, age=?, phone=?, address=?, created_at=?, updated_at=? where id=?");

            pstm1.setString(1, p.getName());
            pstm1.setString(2, p.getCode());
            pstm1.setInt(3, p.getAge());
            pstm1.setString(4, p.getPhone());
            pstm1.setString(5, p.getAddress());
            pstm1.setDate(6, new java.sql.Date(p.getCreated_at().getTime()));
            pstm1.setDate(7, new java.sql.Date(p.getUpdated_at().getTime()));
            pstm1.setInt(8, p.getId());

            pstm1.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(String code) {
        try {
            PreparedStatement pstm = con.prepareStatement("DELETE FROM Students WHERE code=?");
            pstm.setString(1, code);
            pstm.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<SinhVien> searchByNameOrAddress(String nameOrAddress) {

        List<SinhVien> result;
        try {
            PreparedStatement pstm = con.prepareStatement("SELECT * FROM Students WHERE name like ? or address like ?");
            pstm.setString(1, "%" + nameOrAddress + "%");
            pstm.setString(2, "%" + nameOrAddress + "%");
            ResultSet rs = pstm.executeQuery();
            result = ShowInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public List<SinhVien> searchByCode(String code) {
        List<SinhVien> result;
        try {
            PreparedStatement pstm = con.prepareStatement("SELECT * FROM Students WHERE code like ?");
            pstm.setString(1, "%" + code + "%");
            ResultSet rs = pstm.executeQuery();
            result = ShowInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    @Override
    public List<SinhVien> sortByAge() {
        List<SinhVien> result;
        try {
            PreparedStatement pstm = con.prepareStatement("SELECT  * FROM Students ORDER BY age");
            ResultSet rs = pstm.executeQuery();
            result = ShowInfo(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public SinhVien getByCode(String code){
        try {
            PreparedStatement pstm = con.prepareStatement("SELECT * FROM Students WHERE code = ?");
            pstm.setString(1, code);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                SinhVien p = new SinhVien(rs.getInt("id"), rs.getString("name"),
                        rs.getString("code"), rs.getInt("age"), rs.getString("phone"), rs.getString("address"),
                        rs.getDate("created_at"), rs.getDate("updated_at"));
                return p;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}
