package org.example;

import java.util.*;
import java.util.regex.Pattern;

public class Valiadate {
    // lay gia tri code truyen cho biến code bên SinhVien.java
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static Scanner sc = new Scanner(System.in);
    private String code;


    // sắp xếp tên cho đúng khi user nhập nhập sai
    public static String reName(String name) {
        name = name.toLowerCase();
        name = name.trim();
        while (name.indexOf("  ") != -1) {
            name = name.replaceAll("  ", " ");
        }
        String[] temp = name.split(" ");
        name = "";
        for (int i = 0; i < temp.length; i++) {
            name += String.valueOf(temp[i].charAt(0)).toUpperCase() + temp[i].substring(1);
            if (i < temp.length - 1) {
                name += " ";
            }
        }
        return name;
    }

    // Tuổi là số, số nguyên dương lớn hơn 0 nhỏ hơn 200
    public static int checkAge(int age) {
        boolean isContinue = true;

        while (isContinue) {
            try {
                System.out.println("Nhập tuổi của bạn:");
                age = Integer.parseInt(sc.nextLine());
                //Tuổi không hợp lệ khi nhập số âm hoặc lớn hơn 200
                if (age < 0 || age > 200)
                    throw new ArithmeticException("Tuổi không hợp lệ");
                isContinue = false;
            } catch (NumberFormatException e) {
                System.out.println("Tuổi phải là số.");
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
            }
        }
        return age;
    }

    // Hành đông: kiểm tra mã trùng
    public boolean checkCodeDuplicate(String code) {
        SinhVienIplemntDAO p = new SinhVienIplemntDAO(DataBaseConnect.cnt);
        SinhVien a = p.getByCode(code);
        if (a != null) {
            return true;
        }
        return false;
    }

    // Điều kiện để check trùng lặp và check ký tự đặc biệt trong code
    public boolean validateCode() {
        code = sc.nextLine();
        while (true) {
            Boolean isDuplicateCode = checkCodeDuplicate(code);
            Boolean isSpecial = checkCodeSpecialCharacter(code);
            // nếu cả 2 thảo mãn điều kiện thì break
            if (!isDuplicateCode && !isSpecial) {
                break;
            }
            // nếu mã bị trùng thì nhập lại
            if (isDuplicateCode) {
                System.out.println("Mã code bị trùng, cần nhập lại: ");
                code = sc.nextLine();
            }
            if (isSpecial) {
                System.out.print("Mã code ko được có ký tự đặc biệt, yêu cầu nhập lại: ");
                code = sc.nextLine();
            }
        }
        return true;
    }

    // Kiểm tra ký tự đặc biệt trong code
    public Boolean checkCodeSpecialCharacter(String code) {

        Pattern p1 = Pattern.compile("^[a-zA-Z0-9]{1,20}$");
        if (p1.matcher(code).find()) {
            return false;
        }
        return true;
    }

    //Sử dụng riêng cho phần mã code update
    public String checkCodeSpecialCharacter1(String code) {
        Pattern p1 = Pattern.compile("^[a-zA-Z0-9]{1,20}$");
        while (true) {
            if (p1.matcher(code).find()) {
                break;
            } else {
                System.out.print("Mã code ko được có ký tự đặc biệt, yêu cầu nhập lại: ");
                code = sc.nextLine();
            }
        }
        return code;
    }
}
