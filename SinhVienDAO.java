package org.example;

import java.util.List;

public interface SinhVienDAO {
    public List<SinhVien> getAll();

    public void insert(SinhVien p) throws RuntimeException;

    public void update(SinhVien p);

    public void delete(String code);

    public List<SinhVien> searchByNameOrAddress(String nameOrAddress);

    public List<SinhVien> searchByCode(String code);

    public List<SinhVien> sortByAge();
}
