package org.example;

import java.sql.*;

public class DataBaseConnect {
   private static String url = "jdbc:postgresql://localhost:5432/StudentQuanLy";
   private static String user = "postgres";
   private static String passwords = "khongchopassdau1A";
   public static Connection cnt = null;
   Connection getConnection(){

      try{
         cnt = DriverManager.getConnection(url, user, passwords);
         System.out.println("Connected to PostgreSQL!");
      }catch(SQLException e){
         System.out.println(e.getMessage());
      }
      return cnt;
   }
}
