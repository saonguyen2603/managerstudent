package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main extends SinhVien {
    public static List<SinhVien> SinhVienInfo = new ArrayList<>();

    public static void main(String[] args) {
        DataBaseConnect data = new DataBaseConnect();
        data.getConnection();

        SinhVienIplemntDAO p = new SinhVienIplemntDAO(DataBaseConnect.cnt);
        Scanner sc = new Scanner(System.in);


        do {
            System.out.println("1. thêm thông tin sinh viên.");
            System.out.println("2. sửa thông tin sinh viên.");
            System.out.println("3. xoá thông tin sinh viên.");
            System.out.println("4. hiển thị thông tin sinh viên.");
            System.out.println("5. nhập tên hoặc địa chỉ cần tìm.");
            System.out.println("6. nhập mã sinh viên cần tìm.");
            System.out.println("8. sắp xếp từ tuổi sinh viên từ nhỏ tới lớn.");
            System.out.print("Bạn nhập: ");
            int x = sc.nextInt();

            switch (x) {
                case 1:
                    int n;
                    System.out.println("1. Nhập số sinh viên cần thêm: ");
                    n = sc.nextInt();
                    for (int i = 0; i < n; i++) {
                        SinhVien sv = new SinhVien();
                        SinhVien sinhVien = sv.add();
                        p.insert(sinhVien);
                    }
                    break;
                case 2:
                    System.out.println("2. sửa thông tin sinh viên");
                    System.out.print("Nhập id muốn sửa: ");
                    int id = sc.nextInt();
                    for (SinhVien sv : SinhVienInfo) {
                        if (id == sv.getId()) {
                            SinhVien sinhVien1 = sv.update();
                            p.update(sinhVien1);
                        }
                    }
                    break;
                case 3:
                    System.out.println("3. Xoá thông tin sinh viên");
                    sc.nextLine();
                    System.out.print("Nhập code sinh viên cần xoá: ");
                    String code = sc.nextLine();
                    p.delete(code);
                    System.out.println("Xoá thành công");
                    break;
                case 4:
                    SinhVienInfo = p.getAll();
                    for (SinhVien sv1 : SinhVienInfo) {
                        sv1.display();
                    }
                    break;
                case 5:
                    System.out.println("1.Nhập tên hoặc địa chỉ để tìm: ");
                    String nameOrAddress = sc.nextLine();
                    List<SinhVien> result = p.searchByNameOrAddress(nameOrAddress);
                    System.out.println("Kết quả tìm thấy: ");
                    for (int i = 0; i < result.size(); i++) {
                        result.get(i).display();
                    }
                    break;
                case 6:
                    sc.nextLine();
                    System.out.println("Nhập mã sinh viên cần tìm: ");
                    String code1 = sc.nextLine();
                    List<SinhVien> result1 = p.searchByCode(code1);
                    System.out.println("Kết quả tìm thấy: ");
                    for (SinhVien sv1 : result1) {
                        sv1.display();
                    }
                    break;
                case 7:
                    System.out.println("Sắp xếp tuổi sinh viên nhỏ tới lớn.");
                    SinhVienInfo = p.sortByAge();
                    for (SinhVien sv3 : SinhVienInfo) {
                        sv3.display();
                    }
                    break;
                default:
                    System.out.println("Nhập sai vui lòng nhập lại.");
                    break;
            }
        } while (true);
    }
}
